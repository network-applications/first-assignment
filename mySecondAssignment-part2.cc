/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


/* 
 * Second assignment - Ashley Degl'Innocenti
 * May 2023 
 * 
 * Goal: enable and test different Wi-Fi channels (Yans, Multi Model Spectrum) 
 * and show the effect of co-channel interference. 
 * 
 * Network topology:
 *      IEEE 802.11n Wi-Fi networks.
 *      
 *      Wi-Fi 192.168.1.0                            Wi-Fi 192.168.2.0
 *      Wi-Fi Network A                              Wi-Fi Network B
 *      Channel numbers: 
 *      36                                           40 
 *      STA                  AP --------------------AP                 STA
 *       * <-- distance -->  *                      * <-- distance -->  *                
 *       |                   |                      |                   |
 *       n1                  n2 <-- AP distance -- >n3                  n4
 * 
 * Two Wi-Fi networks (working on different logical channels on the same "ns3::YansWifiPhy or s3::SpectrumWifiPhy" channel object).
 * Each network contains one access point and one station. 
 * 
 * There are command-line arguments available concerning: simulation time, udp or tcp, distance, index
 * selection, WIFI TYPE, error model type, enablePcap. To see options 
 * ./ns3 run "mySecondAssignment2 --help"
 * 
 * The program will step through 16 index values for each Wi-Fi network, corresponding to the following MCS, 
 * channel width, and guard interval combinations:
 *  -  index 0-7:    MCS 0-7, long guard interval, 20 MHz channel
 *  -  index 8-15:   MCS 0-7, short guard interval, 20 MHz channel
 * By default, the program sends TCP for 10 seconds using each MCS, using the YansWifiPhy and the
 * NistErrorRateModel, at a distance of 20 meters.  
 */ 

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/boolean.h"
#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/double.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/log.h"
#include "ns3/mobility-helper.h"
#include "ns3/multi-model-spectrum-channel.h"
#include "ns3/on-off-helper.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/packet-sink.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/spectrum-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/string.h"
#include "ns3/udp-client-server-helper.h"
#include "ns3/uinteger.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"
#include <chrono>
#include <iomanip>

using namespace ns3;
using namespace std::chrono; 

// Global variables for use in callbacks.
double g_signalDbmAvg; //!< Average signal power [dBm]
double g_noiseDbmAvg;  //!< Average noise power [dBm]
uint32_t g_samples;    //!< Number of samples

/**
 * Monitor sniffer Rx trace
 *
 * \param packet The sensed packet.
 * \param channelFreqMhz The channel frequency [MHz].
 * \param txVector The Tx vector.
 * \param aMpdu The aMPDU.
 * \param signalNoise The signal and noise dBm.
 * \param staId The STA ID.
 */
void
MonitorSniffRx(Ptr<const Packet> packet,
               uint16_t channelFreqMhz,
               WifiTxVector txVector,
               MpduInfo aMpdu,
               SignalNoiseDbm signalNoise,
               uint16_t staId)

{
    g_samples++;
    g_signalDbmAvg += ((signalNoise.signal - g_signalDbmAvg) / g_samples);
    g_noiseDbmAvg += ((signalNoise.noise - g_noiseDbmAvg) / g_samples);
}


NS_LOG_COMPONENT_DEFINE("SecondAssignment-part2");

int
main(int argc, char* argv[])
{    
    auto startTime = high_resolution_clock::now(); 
    bool udp = false;
    double distance = 20; // meters
    double APdistance = 2; // Distance between APs (meters)
    double simulationTime = 10; // seconds
    uint16_t index = 256;
    std::string wifiType = "YansWifi";
    std::string errorModelType = "ns3::NistErrorRateModel";
    bool enablePcap = false;
    const uint32_t tcpPacketSize = 1448;
    
    // command-line arguments
    CommandLine cmd(__FILE__);
    cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
    cmd.AddValue("udp", "UDP if set to 1, TCP otherwise", udp);
    cmd.AddValue("distance", "meters separation between nodes", distance);
    cmd.AddValue("APdistance", "meters separation between access point nodes", APdistance);
    cmd.AddValue("index", "restrict index to single value between 0 and 15", index);
    cmd.AddValue("wifiType", "select MultiSpectrumWifi or YansWifi", wifiType);
    cmd.AddValue("errorModelType",
                 "select ns3::NistErrorRateModel or ns3::YansErrorRateModel",
                 errorModelType);
    cmd.AddValue("enablePcap", "enable pcap output", enablePcap);
    cmd.Parse(argc, argv);

    uint16_t startIndex = 0;
    uint16_t stopIndex = 15;
    if (index < 16)
    {
        startIndex = index;
        stopIndex = index;
    }

    std::cout << "Values displayed for network A." << std::endl;
    std::cout << "wifiType: " << wifiType << " distance: " << distance
              << "m; time: " << simulationTime << "; TxPower: 1 dBm (1.3 mW); udp: " 
              << udp << std::endl;
    std::cout << std::setw(5) << "index" << std::setw(6) << "MCS" << std::setw(13) << "Rate (Mb/s)"
              << std::setw(12) << "Tput (Mb/s)" << std::setw(10) << "Received " << std::setw(12)
              << "Signal (dBm)" << std::setw(12) << "Noise (dBm)" << std::setw(9) << "SNR (dB)"
              << std::endl;
    for (uint16_t i = startIndex; i <= stopIndex; i++)
    {
        uint32_t payloadSize;
        if (udp)
        {
            payloadSize = 972; // 1000 bytes IPv4
        }
        else
        {
            payloadSize = 1448; // 1500 bytes IPv6
            Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(payloadSize));
        } 
        //Configure global network topology 
         NodeContainer wifiStaNodes;
        wifiStaNodes.Create(2);
        NodeContainer wifiApNodes;
        wifiApNodes.Create(2);

        NetDeviceContainer staDeviceA;
        NetDeviceContainer staDeviceB;
        NetDeviceContainer apDeviceA;
        NetDeviceContainer apDeviceB;

        // Create a phy helper 
        YansWifiPhyHelper phy;
        SpectrumWifiPhyHelper multiSpectrumPhy; 

        // Choose Wi-Fi type and then create the channel helper
        if (wifiType == "YansWifi")
        {
            YansWifiChannelHelper channel;
            channel.AddPropagationLoss("ns3::FriisPropagationLossModel",
                                       "Frequency",
                                       DoubleValue(5.180e9));
            channel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
            phy.SetChannel(channel.Create());
            phy.Set("TxPowerStart", DoubleValue(1)); // dBm (1.26 mW)
            phy.Set("TxPowerEnd", DoubleValue(1));
        }
        else if (wifiType == "MultiSpectrumWifi")
        {
            Ptr<MultiModelSpectrumChannel> multiSpectrumChannel = CreateObject<MultiModelSpectrumChannel>();
            Ptr<FriisPropagationLossModel> lossModel = CreateObject<FriisPropagationLossModel>();
            lossModel->SetFrequency(5.180e9);
            multiSpectrumChannel->AddPropagationLossModel(lossModel);

            Ptr<ConstantSpeedPropagationDelayModel> delayModel =
                CreateObject<ConstantSpeedPropagationDelayModel>();
            multiSpectrumChannel->SetPropagationDelayModel(delayModel);

            multiSpectrumPhy.SetChannel(multiSpectrumChannel);
            multiSpectrumPhy.SetErrorRateModel(errorModelType);
            multiSpectrumPhy.Set("TxPowerStart", DoubleValue(1)); // dBm  (1.26 mW)
            multiSpectrumPhy.Set("TxPowerEnd", DoubleValue(1));
        }
        else
        {
            NS_FATAL_ERROR("Unsupported WiFi type " << wifiType);
        }

        // Create a WifiHelper, which will use other helpers to create and istall Wifi devices. 
        // Configure a Wifi standard to use 
        // Create a WifiMacHelper 
        WifiHelper wifi;
        wifi.SetStandard(WIFI_STANDARD_80211n);
        WifiMacHelper mac; 
        // Wi-Fi Network A
        Ssid ssid1= Ssid("network-A");
        // Wi-Fi Network B     
        Ssid ssid2 = Ssid("network-B"); 

        double datarate = 0;
        StringValue DataRate;
        if (i == 0)
        {
            DataRate = StringValue("HtMcs0");
            datarate = 6.5;
        }
        else if (i == 1)
        {
            DataRate = StringValue("HtMcs1");
            datarate = 13;
        }
        else if (i == 2)
        {
            DataRate = StringValue("HtMcs2");
            datarate = 19.5;
        }
        else if (i == 3)
        {
            DataRate = StringValue("HtMcs3");
            datarate = 26;
        }
        else if (i == 4)
        {
            DataRate = StringValue("HtMcs4");
            datarate = 39;
        }
        else if (i == 5)
        {
            DataRate = StringValue("HtMcs5");
            datarate = 52;
        }
        else if (i == 6)
        {
            DataRate = StringValue("HtMcs6");
            datarate = 58.5;
        }
        else if (i == 7)
        {
            DataRate = StringValue("HtMcs7");
            datarate = 65;
        }
        else if (i == 8)
        {
            DataRate = StringValue("HtMcs0");
            datarate = 7.2;
        }
        else if (i == 9)
        {
            DataRate = StringValue("HtMcs1");
            datarate = 14.4;
        }
        else if (i == 10)
        {
            DataRate = StringValue("HtMcs2");
            datarate = 21.7;
        }
        else if (i == 11)
        {
            DataRate = StringValue("HtMcs3");
            datarate = 28.9;
        }
        else if (i == 12)
        {
            DataRate = StringValue("HtMcs4");
            datarate = 43.3;
        }
        else if (i == 13)
        {
            DataRate = StringValue("HtMcs5");
            datarate = 57.8;
        }
        else if (i == 14)
        {
            DataRate = StringValue("HtMcs6");
            datarate = 65;
        }
        else if (i == 15)
        {
            DataRate = StringValue("HtMcs7");
            datarate = 72.2;
        }

        wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager",
                                     "DataMode",
                                     DataRate,
                                     "ControlMode",
                                     DataRate);

        // select wifiType and perform the installation 
        if (wifiType == "YansWifi")
        {   
            //Install PHY and MAC
            // Wi-Fi Network A
            mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid1));
            phy.Set("ChannelSettings", StringValue("{36, 20, BAND_5GHZ, 0}"));
            staDeviceA = wifi.Install(phy, mac, wifiStaNodes.Get(0));
            mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid1));
            apDeviceA = wifi.Install(phy, mac, wifiApNodes.Get(0));

            // Wi-Fi Network B
            mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid2));
            phy.Set("ChannelSettings", StringValue("{40, 20, BAND_5GHZ, 0}"));
            staDeviceB = wifi.Install(phy, mac, wifiStaNodes.Get(1));
            mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid2));
            apDeviceB = wifi.Install(phy, mac, wifiApNodes.Get(1));
        }
        else if (wifiType == "MultiSpectrumWifi")
        {
            //Install PHY and MAC
            // Wi-Fi Network A
            mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid1));
            multiSpectrumPhy.Set("ChannelSettings", StringValue("{36, 20, BAND_5GHZ, 0}"));
            staDeviceA = wifi.Install(multiSpectrumPhy, mac, wifiStaNodes.Get(0));
            mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid1));
            apDeviceA = wifi.Install(multiSpectrumPhy, mac, wifiApNodes.Get(0));
            
            // Wi-Fi Network B
            mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid2));
            multiSpectrumPhy.Set("ChannelSettings", StringValue("{40, 20, BAND_5GHZ, 0}"));
            staDeviceB = wifi.Install(multiSpectrumPhy, mac, wifiStaNodes.Get(1));
            mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid2));
            apDeviceB = wifi.Install(multiSpectrumPhy, mac, wifiApNodes.Get(1));
        }


        if (i <= 7)
        {
            Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HtConfiguration/"
                        "ShortGuardIntervalSupported",
                        BooleanValue(false));
        }
        else if (i > 7 && i <= 15)
        {
            Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HtConfiguration/"
                        "ShortGuardIntervalSupported",
                        BooleanValue(true));
        }

        // Mobility model configuration

        MobilityHelper mobility; 
        Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator>();
        // AP nodes and STA nodes are in fixed position
        mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");

        //Set position STA nodes
        positionAlloc->Add(Vector(0.0, 0.0, 0.0));
        positionAlloc->Add(Vector(2*distance+APdistance, 0.0, 0.0));
        
        //Set position for AP nodes
        positionAlloc->Add(Vector(distance, 0.0, 0.0));        
        positionAlloc->Add(Vector(distance+APdistance, 0.0, 0.0));
        

        mobility.SetPositionAllocator(positionAlloc);
        mobility.Install(wifiStaNodes);
        mobility.Install(wifiApNodes);
        

        // Internet stack
        InternetStackHelper stack;
        stack.Install(wifiApNodes);
        stack.Install(wifiStaNodes);

        // Add IP addresses to our device interfaces
        Ipv4AddressHelper address; 

        // Wi-Fi Network A
        address.SetBase("192.168.1.0", "255.255.255.0");
        Ipv4InterfaceContainer staNodeInterfaceA;
        Ipv4InterfaceContainer apNodeInterfaceA;
        staNodeInterfaceA = address.Assign(staDeviceA);
        apNodeInterfaceA = address.Assign(apDeviceA);

        // Wi-Fi Network B
        address.SetBase("192.168.2.0", "255.255.255.0");
        Ipv4InterfaceContainer staNodeInterfaceB;
        Ipv4InterfaceContainer apNodeInterfaceB;
        staNodeInterfaceB = address.Assign(staDeviceB);
        apNodeInterfaceB = address.Assign(apDeviceB);

        // Setting up applications
        ApplicationContainer serverApp1;
        ApplicationContainer serverApp2;
        if (udp)
        {
            // UDP flow
            // Wi-Fi Network A
            uint16_t port = 9;
            UdpServerHelper server1(port);
            serverApp1 = server1.Install(wifiStaNodes.Get(0));
            serverApp1.Start(Seconds(0.0));
            serverApp1.Stop(Seconds(simulationTime + 1));

            UdpClientHelper client1(staNodeInterfaceA.GetAddress(0), port);
            client1.SetAttribute("MaxPackets", UintegerValue(4294967295U));
            client1.SetAttribute("Interval", TimeValue(Time("0.0001"))); // packets/s
            client1.SetAttribute("PacketSize", UintegerValue(payloadSize));
            ApplicationContainer clientApp1 = client1.Install(wifiApNodes.Get(0));
            clientApp1.Start(Seconds(1.0));
            clientApp1.Stop(Seconds(simulationTime + 1));

            // Wi-Fi Network B
            UdpServerHelper server2(port);
            serverApp2 = server2.Install(wifiStaNodes.Get(1));
            serverApp2.Start(Seconds(0.0));
            serverApp2.Stop(Seconds(simulationTime + 1));

            UdpClientHelper client2(staNodeInterfaceB.GetAddress(0), port);
            client2.SetAttribute("MaxPackets", UintegerValue(4294967295U));
            client2.SetAttribute("Interval", TimeValue(Time("0.0001"))); // packets/s
            client2.SetAttribute("PacketSize", UintegerValue(payloadSize));
            ApplicationContainer clientApp2 = client2.Install(wifiApNodes.Get(1));
            clientApp2.Start(Seconds(1.0));
            clientApp2.Stop(Seconds(simulationTime + 1));
        }
        else
        {
            // TCP flow
            // Wi-Fi Network A
            uint16_t port = 50000;
            Address localAddress1(InetSocketAddress(Ipv4Address::GetAny(), port));
            PacketSinkHelper packetSinkHelper1("ns3::TcpSocketFactory", localAddress1);
            serverApp1 = packetSinkHelper1.Install(wifiStaNodes.Get(0));
            serverApp1.Start(Seconds(0.0));
            serverApp1.Stop(Seconds(simulationTime + 1));

            OnOffHelper onoff1("ns3::TcpSocketFactory", Ipv4Address::GetAny());
            onoff1.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
            onoff1.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
            onoff1.SetAttribute("PacketSize", UintegerValue(payloadSize));
            onoff1.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
            AddressValue remoteAddress1(InetSocketAddress(staNodeInterfaceA.GetAddress(0), port));
            onoff1.SetAttribute("Remote", remoteAddress1);
            ApplicationContainer clientApp1 = onoff1.Install(wifiApNodes.Get(0));
            clientApp1.Start(Seconds(1.0));
            clientApp1.Stop(Seconds(simulationTime + 1));

            // Wi-Fi Network B
            Address localAddress2(InetSocketAddress(Ipv4Address::GetAny(), port));
            PacketSinkHelper packetSinkHelper2("ns3::TcpSocketFactory", localAddress2);
            serverApp2 = packetSinkHelper2.Install(wifiStaNodes.Get(1));
            serverApp2.Start(Seconds(0.0));
            serverApp2.Stop(Seconds(simulationTime + 1));

            OnOffHelper onoff2("ns3::TcpSocketFactory", Ipv4Address::GetAny());
            onoff2.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
            onoff2.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
            onoff2.SetAttribute("PacketSize", UintegerValue(payloadSize));
            onoff2.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
            AddressValue remoteAddress2(InetSocketAddress(staNodeInterfaceB.GetAddress(0), port));
            onoff2.SetAttribute("Remote", remoteAddress2);
            ApplicationContainer clientApp2 = onoff2.Install(wifiApNodes.Get(1));
            clientApp2.Start(Seconds(1.0));
            clientApp2.Stop(Seconds(simulationTime + 1));
        }

        Config::ConnectWithoutContext("/NodeList/0/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback(&MonitorSniffRx));


        // Enable PCAP generation on AP node of network A
        if (enablePcap)
        {
            if (wifiType == "YansWifi")
            {   
                phy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
                std::stringstream ss;
                ss << "secondAssignment_Yans_2AP-" << i;
                phy.EnablePcap(ss.str(), apDeviceA);
            }
            else if (wifiType == "MultiSpectrumWifi")
            {   
                multiSpectrumPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
                std::stringstream ss;
                ss << "secondAssignment_Multi_2AP-" << i;
                multiSpectrumPhy.EnablePcap(ss.str(), apDeviceA);
            }
        }
        g_signalDbmAvg = 0;
        g_noiseDbmAvg = 0;
        g_samples = 0;

        Simulator::Stop(Seconds(simulationTime + 1));
        Simulator::Run();

        double throughput = 0;
        uint64_t totalPacketsThrough = 0;
        // Output for Wi-Fi Network A
        if (udp)
        {
            // UDP
            totalPacketsThrough = DynamicCast<UdpServer>(serverApp1.Get(0))->GetReceived();
            throughput =
                totalPacketsThrough * payloadSize * 8 / (simulationTime * 1000000.0); // Mbit/s
        }
        else
        {
            // TCP
            uint64_t totalBytesRx = DynamicCast<PacketSink>(serverApp1.Get(0))->GetTotalRx();
            totalPacketsThrough = totalBytesRx / tcpPacketSize;
            throughput = totalBytesRx * 8 / (simulationTime * 1000000.0); // Mbit/s
        }
        std::cout << std::setw(5) << i << std::setw(6) << (i % 8) << std::setprecision(2)
                  << std::fixed << std::setw(10) << datarate << std::setw(12) << throughput
                  << std::setw(8) << totalPacketsThrough;
        if (totalPacketsThrough > 0)
        {
            std::cout << std::setw(12) << g_signalDbmAvg << std::setw(12) << g_noiseDbmAvg
                      << std::setw(12) << (g_signalDbmAvg - g_noiseDbmAvg) << std::endl;
        }
        else
        {
            std::cout << std::setw(12) << "N/A" << std::setw(12) << "N/A" << std::setw(12) << "N/A"
                      << std::endl;
        }
        
        Simulator::Destroy();
    }

    // Execution time
    auto stopTime = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stopTime - startTime);
    auto durationSEC= duration_cast<seconds>(duration);
    std::cout << "Time taken for simulation execution: "  << duration.count() << " microseconds." << std::endl;
    std::cout << "Time taken for simulation execution: "  << durationSEC.count() << " seconds." << std::endl;
    return 0;
}

