/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


/* 
 * Second assignment - Ashley Degl'Innocenti
 * May 2023 
 * 
 * Goal: enable and test different Wi-Fi channels (Yans, Multi Model Spectrum, Single Model Spectrum)
 * Network topology:
 *  IEEE 802.11n Wi-Fi network.
 *  Wi-Fi 192.168.1.0
 *      STA                  AP
 *       * <-- distance -->  *
 *       |                   |
 *       n1                  n2
 * 
 * There are command-line arguments available concerning: simulation time, udp or tcp, distance, index
 * selection, WIFI TYPE, error model type, enablePcap. To see options: 
 * ./ns3 run "mySecondAssignment2 --help"
 * 
 * The program will step through 16 index values, corresponding to the following MCS, 
 * channel width, and guard interval combinations:
 *  -  index 0-7:    MCS 0-7, long guard interval, 20 MHz channel
 *  -  index 8-15:   MCS 0-7, short guard interval, 20 MHz channel
 * By default, the program sends TCP for 10 seconds using each MCS, using the YansWifiPhy and the
 * NistErrorRateModel, at a distance of 20 meters.  
 */ 

#include "ns3/boolean.h"
#include "ns3/command-line.h" 
#include "ns3/config.h"
#include "ns3/double.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/log.h"
#include "ns3/mobility-helper.h"
#include "ns3/multi-model-spectrum-channel.h"
#include "ns3/single-model-spectrum-channel.h"
#include "ns3/on-off-helper.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/packet-sink.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/spectrum-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/string.h"
#include "ns3/udp-client-server-helper.h"
#include "ns3/uinteger.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"
#include <chrono>
#include <iomanip>

using namespace ns3;
using namespace std::chrono; 

// Global variables for use in callbacks.
double g_signalDbmAvg; //!< Average signal power [dBm]
double g_noiseDbmAvg;  //!< Average noise power [dBm]
uint32_t g_samples;    //!< Number of samples

/**
 * Monitor sniffer Rx trace
 *
 * \param packet The sensed packet.
 * \param channelFreqMhz The channel frequency [MHz].
 * \param txVector The Tx vector.
 * \param aMpdu The aMPDU.
 * \param signalNoise The signal and noise dBm.
 * \param staId The STA ID.
 */
void
MonitorSniffRx(Ptr<const Packet> packet,
               uint16_t channelFreqMhz,
               WifiTxVector txVector,
               MpduInfo aMpdu,
               SignalNoiseDbm signalNoise,
               uint16_t staId)

{
    g_samples++;
    g_signalDbmAvg += ((signalNoise.signal - g_signalDbmAvg) / g_samples);
    g_noiseDbmAvg += ((signalNoise.noise - g_noiseDbmAvg) / g_samples);
}

NS_LOG_COMPONENT_DEFINE("SecondAssignment");

int
main(int argc, char* argv[])
{
    auto startTime = high_resolution_clock::now(); 
    bool udp = false;
    double distance = 20; // meters
    double simulationTime = 10; // seconds
    uint16_t index = 256;
    std::string wifiType = "YansWifi";
    std::string errorModelType = "ns3::NistErrorRateModel";
    bool enablePcap = false;
    const uint32_t tcpPacketSize = 1448;
    
    // command-line arguments
    CommandLine cmd(__FILE__);
    cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
    cmd.AddValue("udp", "UDP if set to 1, TCP otherwise", udp);
    cmd.AddValue("distance", "meters separation between nodes", distance);
    cmd.AddValue("index", "restrict index to single value between 0 and 15", index);
    cmd.AddValue("wifiType", "select MultiSpectrumWifi, SingleSpectrumWifi or YansWifi", wifiType);
    cmd.AddValue("errorModelType",
                 "select ns3::NistErrorRateModel or ns3::YansErrorRateModel",
                 errorModelType);
    cmd.AddValue("enablePcap", "enable pcap output", enablePcap);
    cmd.Parse(argc, argv);

    uint16_t startIndex = 0;
    uint16_t stopIndex = 15;
    if (index < 16)
    {
        startIndex = index;
        stopIndex = index;
    }

    std::cout << "wifiType: " << wifiType << " distance: " << distance
              << "m; time: " << simulationTime << "; TxPower: 1 dBm (1.3 mW); udp: " 
              << udp << std::endl;
    std::cout << std::setw(5) << "index" << std::setw(6) << "MCS" << std::setw(13) << "Rate (Mb/s)"
              << std::setw(12) << "Tput (Mb/s)" << std::setw(10) << "Received " << std::setw(12)
              << "Signal (dBm)" << std::setw(12) << "Noise (dBm)" << std::setw(9) << "SNR (dB)"
              << std::endl;
    for (uint16_t i = startIndex; i <= stopIndex; i++)
    {
        uint32_t payloadSize;
        if (udp)
        {
            payloadSize = 972; // 1000 bytes IPv4
        }
        else
        {
            payloadSize = 1448; // 1500 bytes IPv6
            Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(payloadSize));
        }

        NodeContainer wifiStaNode;
        wifiStaNode.Create(1); // Create 1 station node object    
        NodeContainer wifiApNode;
        wifiApNode.Create(1); // Create 1 access point node object 

        // Create a phy helper 
        YansWifiPhyHelper phy;
        SpectrumWifiPhyHelper singleSpectrumPhy;
        SpectrumWifiPhyHelper multiSpectrumPhy;
        // Choose Wi-Fi type and then create the channel helper
        if (wifiType == "YansWifi")
        {
            YansWifiChannelHelper channel;
            channel.AddPropagationLoss("ns3::FriisPropagationLossModel",
                                       "Frequency",
                                       DoubleValue(5.180e9));
            channel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
            phy.SetChannel(channel.Create());
            phy.Set("TxPowerStart", DoubleValue(1)); // dBm (1.26 mW)
            phy.Set("TxPowerEnd", DoubleValue(1));
        }
        else if (wifiType == "MultiSpectrumWifi")
        {
            Ptr<MultiModelSpectrumChannel> multiSpectrumChannel = CreateObject<MultiModelSpectrumChannel>();
            Ptr<FriisPropagationLossModel> lossModel = CreateObject<FriisPropagationLossModel>();
            lossModel->SetFrequency(5.180e9);
            multiSpectrumChannel->AddPropagationLossModel(lossModel);

            Ptr<ConstantSpeedPropagationDelayModel> delayModel =
                CreateObject<ConstantSpeedPropagationDelayModel>();
            multiSpectrumChannel->SetPropagationDelayModel(delayModel);

            multiSpectrumPhy.SetChannel(multiSpectrumChannel);
            multiSpectrumPhy.SetErrorRateModel(errorModelType);
            multiSpectrumPhy.Set("TxPowerStart", DoubleValue(1)); // dBm  (1.26 mW)
            multiSpectrumPhy.Set("TxPowerEnd", DoubleValue(1));
        }
        else if (wifiType == "SingleSpectrumWifi")
        {
            Ptr<SingleModelSpectrumChannel> singleSpectrumChannel = CreateObject<SingleModelSpectrumChannel>();
            //Same loss model and propagation delay than Multi Model Spectrum Channel 
            Ptr<FriisPropagationLossModel> lossModel = CreateObject<FriisPropagationLossModel>();
            lossModel->SetFrequency(5.180e9);
            singleSpectrumChannel->AddPropagationLossModel(lossModel);
            Ptr<ConstantSpeedPropagationDelayModel> delayModel =
                CreateObject<ConstantSpeedPropagationDelayModel>();
            singleSpectrumChannel->SetPropagationDelayModel(delayModel);

            singleSpectrumPhy.SetChannel(singleSpectrumChannel);
            singleSpectrumPhy.SetErrorRateModel(errorModelType);
            singleSpectrumPhy.Set("TxPowerStart", DoubleValue(1)); // dBm  (1.26 mW)
            singleSpectrumPhy.Set("TxPowerEnd", DoubleValue(1)); 
        }
        else
        {
            NS_FATAL_ERROR("Unsupported WiFi type " << wifiType);
        }

        // Create a WifiHelper, which will use other helpers to create and istall Wifi devices. 
        // Configure a Wifi standard to use 
        // Create a WifiMacHelper 
        WifiHelper wifi;
        wifi.SetStandard(WIFI_STANDARD_80211n);
        WifiMacHelper mac;

        Ssid ssid = Ssid("ns380211n");

        double datarate = 0;
        StringValue DataRate;
        if (i == 0)
        {
            DataRate = StringValue("HtMcs0");
            datarate = 6.5;
        }
        else if (i == 1)
        {
            DataRate = StringValue("HtMcs1");
            datarate = 13;
        }
        else if (i == 2)
        {
            DataRate = StringValue("HtMcs2");
            datarate = 19.5;
        }
        else if (i == 3)
        {
            DataRate = StringValue("HtMcs3");
            datarate = 26;
        }
        else if (i == 4)
        {
            DataRate = StringValue("HtMcs4");
            datarate = 39;
        }
        else if (i == 5)
        {
            DataRate = StringValue("HtMcs5");
            datarate = 52;
        }
        else if (i == 6)
        {
            DataRate = StringValue("HtMcs6");
            datarate = 58.5;
        }
        else if (i == 7)
        {
            DataRate = StringValue("HtMcs7");
            datarate = 65;
        }
        else if (i == 8)
        {
            DataRate = StringValue("HtMcs0");
            datarate = 7.2;
        }
        else if (i == 9)
        {
            DataRate = StringValue("HtMcs1");
            datarate = 14.4;
        }
        else if (i == 10)
        {
            DataRate = StringValue("HtMcs2");
            datarate = 21.7;
        }
        else if (i == 11)
        {
            DataRate = StringValue("HtMcs3");
            datarate = 28.9;
        }
        else if (i == 12)
        {
            DataRate = StringValue("HtMcs4");
            datarate = 43.3;
        }
        else if (i == 13)
        {
            DataRate = StringValue("HtMcs5");
            datarate = 57.8;
        }
        else if (i == 14)
        {
            DataRate = StringValue("HtMcs6");
            datarate = 65;
        }
        else if (i == 15)
        {
            DataRate = StringValue("HtMcs7");
            datarate = 72.2;
        }

        wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager",
                                     "DataMode",
                                     DataRate,
                                     "ControlMode",
                                     DataRate);
        
        // Declare NetDeviceContainers to hold the container returned by the helper
        NetDeviceContainer staDevice;
        NetDeviceContainer apDevice;

        // select wifiType and perform the installation 
        if (wifiType == "YansWifi")
        {   
            //Install PHY and MAC
            mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
            phy.Set("ChannelSettings", StringValue("{0, 20, BAND_5GHZ, 0}"));
            staDevice = wifi.Install(phy, mac, wifiStaNode);
            mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
            apDevice = wifi.Install(phy, mac, wifiApNode);
        }
        else if (wifiType == "MultiSpectrumWifi")
        {
            //Install PHY and MAC
            mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
            multiSpectrumPhy. Set("ChannelSettings", StringValue("{0, 20, BAND_5GHZ, 0}"));//primo 0 è il numero di canale
            staDevice = wifi.Install(multiSpectrumPhy, mac, wifiStaNode);
            mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
            apDevice = wifi.Install(multiSpectrumPhy, mac, wifiApNode);
        }
        else if (wifiType == "SingleSpectrumWifi")
        {
            //Install PHY and MAC
            mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
            singleSpectrumPhy.Set("ChannelSettings", StringValue("{0, 20, BAND_5GHZ, 0}"));
            staDevice = wifi.Install(singleSpectrumPhy, mac, wifiStaNode);
            mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
            apDevice = wifi.Install(singleSpectrumPhy, mac, wifiApNode);
        }

        if (i <= 7)
        {
            Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HtConfiguration/"
                        "ShortGuardIntervalSupported",
                        BooleanValue(false));
        }
        else if (i > 7 && i <= 15)
        {
            Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HtConfiguration/"
                        "ShortGuardIntervalSupported",
                        BooleanValue(true));
        }

        // Mobility model configuration
        MobilityHelper mobility;
        Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator>();

        positionAlloc->Add(Vector(0.0, 0.0, 0.0));
        positionAlloc->Add(Vector(distance, 0.0, 0.0));
        mobility.SetPositionAllocator(positionAlloc);

        
        mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");

        // AP node and STA node are in fixed positions
        mobility.Install(wifiApNode);
        mobility.Install(wifiStaNode);

        // Internet stack
        InternetStackHelper stack;
        stack.Install(wifiApNode);
        stack.Install(wifiStaNode);
        
        // Add IP addresses to our device interfaces
        Ipv4AddressHelper address;
        address.SetBase("192.168.1.0", "255.255.255.0");
        Ipv4InterfaceContainer staNodeInterface;
        Ipv4InterfaceContainer apNodeInterface;

        staNodeInterface = address.Assign(staDevice);
        apNodeInterface = address.Assign(apDevice);

        // Setting up applications
        ApplicationContainer serverApp;
        if (udp)
        {
            // UDP flow
            uint16_t port = 9;
            UdpServerHelper server(port);
            serverApp = server.Install(wifiStaNode.Get(0));
            serverApp.Start(Seconds(0.0));
            serverApp.Stop(Seconds(simulationTime + 1));

            UdpClientHelper client(staNodeInterface.GetAddress(0), port);
            client.SetAttribute("MaxPackets", UintegerValue(4294967295U));
            client.SetAttribute("Interval", TimeValue(Time("0.0001"))); // packets/s
            client.SetAttribute("PacketSize", UintegerValue(payloadSize));
            ApplicationContainer clientApp = client.Install(wifiApNode.Get(0));
            clientApp.Start(Seconds(1.0));
            clientApp.Stop(Seconds(simulationTime + 1));
        }
        else
        {
            // TCP flow
            uint16_t port = 50000;
            Address localAddress(InetSocketAddress(Ipv4Address::GetAny(), port));
            PacketSinkHelper packetSinkHelper("ns3::TcpSocketFactory", localAddress);
            serverApp = packetSinkHelper.Install(wifiStaNode.Get(0));
            serverApp.Start(Seconds(0.0));
            serverApp.Stop(Seconds(simulationTime + 1));

            OnOffHelper onoff("ns3::TcpSocketFactory", Ipv4Address::GetAny());
            onoff.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
            onoff.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
            onoff.SetAttribute("PacketSize", UintegerValue(payloadSize));
            onoff.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
            AddressValue remoteAddress(InetSocketAddress(staNodeInterface.GetAddress(0), port));
            onoff.SetAttribute("Remote", remoteAddress);
            ApplicationContainer clientApp = onoff.Install(wifiApNode.Get(0));
            clientApp.Start(Seconds(1.0));
            clientApp.Stop(Seconds(simulationTime + 1));
        }

        Config::ConnectWithoutContext("/NodeList/0/DeviceList/*/Phy/MonitorSnifferRx",
                                      MakeCallback(&MonitorSniffRx));

        if (enablePcap)
        {
            if (wifiType == "YansWifi")
            {   
                phy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
                std::stringstream ss;
                ss << "secondAssignment_Yans-" << i;
                phy.EnablePcap(ss.str(), apDevice);
            }
            else if (wifiType == "MultiSpectrumWifi")
            {   
                multiSpectrumPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
                std::stringstream ss;
                ss << "secondAssignment_Multi-" << i;
                multiSpectrumPhy.EnablePcap(ss.str(), apDevice);
            }
            else if (wifiType == "SingleSpectrumWifi")
            {
                singleSpectrumPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
                std::stringstream ss;
                ss << "secondAssignment_Single-" << i;
                singleSpectrumPhy.EnablePcap(ss.str(), apDevice);
            }
        }
        g_signalDbmAvg = 0;
        g_noiseDbmAvg = 0;
        g_samples = 0;

        Simulator::Stop(Seconds(simulationTime + 1));
        Simulator::Run();

        double throughput = 0;
        uint64_t totalPacketsThrough = 0;
        if (udp)
        {
            // UDP
            totalPacketsThrough = DynamicCast<UdpServer>(serverApp.Get(0))->GetReceived();
            throughput =
                totalPacketsThrough * payloadSize * 8 / (simulationTime * 1000000.0); // Mbit/s
        }
        else
        {
            // TCP
            uint64_t totalBytesRx = DynamicCast<PacketSink>(serverApp.Get(0))->GetTotalRx();
            totalPacketsThrough = totalBytesRx / tcpPacketSize;
            throughput = totalBytesRx * 8 / (simulationTime * 1000000.0); // Mbit/s
        }
        std::cout << std::setw(5) << i << std::setw(6) << (i % 8) << std::setprecision(2)
                  << std::fixed << std::setw(10) << datarate << std::setw(12) << throughput
                  << std::setw(8) << totalPacketsThrough;
        if (totalPacketsThrough > 0)
        {
            std::cout << std::setw(12) << g_signalDbmAvg << std::setw(12) << g_noiseDbmAvg
                      << std::setw(12) << (g_signalDbmAvg - g_noiseDbmAvg) << std::endl;
        }
        else
        {
            std::cout << std::setw(12) << "N/A" << std::setw(12) << "N/A" << std::setw(12) << "N/A"
                      << std::endl;
        }
        
        Simulator::Destroy();
    }

    // Execution time
    auto stopTime = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stopTime - startTime);
    auto durationSEC= duration_cast<seconds>(duration);
    std::cout << "Time taken for simulation execution: "  << duration.count() << " microseconds." << std::endl;
    std::cout << "Time taken for simulation execution: "  << durationSEC.count() << " seconds." << std::endl;
    return 0;
}
