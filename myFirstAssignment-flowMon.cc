/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* 
 * First assignment - Ashley Degl'Innocenti
 * May 2023 
 *  
 * Network simulation with:
 * - at least 4 nodes
 * - node 0 communicates with node 1
 * - node 2 communicates with node 3
 * - Find: RTT, Throughput, Packet loss (I'll be using UDP)
 * 
 * Network topology
 *       C     S    C    S
 *       n0    n1   n2   n3
 *       |     |    |    |
 *       =================
 *           LAN (csma)
 * C stands for client
 * S stands for server
 * 
 */ 

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/flow-monitor-module.h"

#include <fstream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("FirstAssignment");

int
main(int argc, char* argv[])
{
    bool verbose = true;
    uint32_t nCsma = 4;
    Address server1Address; 
    Address server2Address;

    // Variable declaration for Flow Monitor
    uint32_t SentPackets = 0;
    uint32_t ReceivedPackets = 0;
    uint32_t LostPackets = 0;
    
    
    CommandLine cmd(__FILE__);
    cmd.AddValue("nCsma", "Number of \"extra\" CSMA nodes/devices", nCsma);
    cmd.AddValue("verbose", "Tell echo applications to log if true", verbose);
    cmd.Parse(argc, argv); 

    // User may find it convenient to turn on explicit debugging 
    if (verbose) 
    {
        LogComponentEnable ("FirstAssignment", LOG_LEVEL_INFO);
        LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
        LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);
    }

    nCsma = nCsma == 0 ? 1 : nCsma; 
    
    NS_LOG_INFO("Create nodes.");
    // Nodes creation
    // Declare NodeContainer to hold the nodes that will be part of the CSMA network
    NodeContainer csmaNodes;
    csmaNodes.Create(nCsma);

    NS_LOG_INFO("Create channels.");
    // Explicitly create the channels required by the topology (shown above)
    // Set the data rate, the speed-of-light delay of the channel, the MTU
    CsmaHelper csma;
    csma.SetChannelAttribute("DataRate", StringValue("100Mbps"));
    csma.SetChannelAttribute("Delay", TimeValue(NanoSeconds(6560))); 
    csma.SetDeviceAttribute("Mtu", UintegerValue(1400));

    // Create NetDeviceContainer to hold the devices created by our CsmaHelper. 
    // Call the Install method of the CsmaHelper to install the devices into the nodes of the csmaNodes NodeContainer.
    
    NetDeviceContainer csmaDevices;
    csmaDevices = csma.Install(csmaNodes);

    // Use InternetStackHelper to install protocol stack stacks.
    InternetStackHelper stack;
    stack.Install(csmaNodes); 

    // We've got the "hardware" in place.  Now we need to add IP addresses to our device interfaces
    NS_LOG_INFO("Assign IP Addresses.");
    Ipv4AddressHelper address;
    address.SetBase("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer csmaInterfaces;
    csmaInterfaces = address.Assign(csmaDevices);
    server1Address = csmaInterfaces.GetAddress(1);
    server2Address = csmaInterfaces.GetAddress(3);


    NS_LOG_INFO("Create Applications.");
    // Create a UdpEchoServer application on node one.
    
    uint16_t port = 9; // well-known echo port number
    UdpEchoServerHelper server1(port);
    ApplicationContainer apps1 = server1.Install(csmaNodes.Get(1));
    apps1.Start(Seconds(1.0));
    apps1.Stop(Seconds(100.0));

    // Create a UdpEchoServer application on node three.
    
    UdpEchoServerHelper server2(port);
    ApplicationContainer apps2 = server2.Install(csmaNodes.Get(3));
    apps2.Start(Seconds(1.0));
    apps2.Stop(Seconds(100.0));

    
    // Create a UdpEchoClient application to send UDP datagrams from node zero to node one.
    
    uint32_t packetSize = 1024;
    uint32_t maxPacketCount = 100;
    Time interPacketInterval = Seconds(1.);
    UdpEchoClientHelper client1(server1Address, port);
    client1.SetAttribute("MaxPackets", UintegerValue(maxPacketCount));
    client1.SetAttribute("Interval", TimeValue(interPacketInterval));
    client1.SetAttribute("PacketSize", UintegerValue(packetSize));
    apps1 = client1.Install(csmaNodes.Get(0));
    apps1.Start(Seconds(2.0));
    apps1.Stop(Seconds(100.0));

    // Create a UdpEchoClient application to send UDP datagrams from node two to node three.
    
    UdpEchoClientHelper client2(server2Address, port);
    client2.SetAttribute("MaxPackets", UintegerValue(maxPacketCount));
    client2.SetAttribute("Interval", TimeValue(interPacketInterval));
    client2.SetAttribute("PacketSize", UintegerValue(packetSize));
    apps2 = client2.Install(csmaNodes.Get(2));
    apps2.Start(Seconds(2.0));
    apps2.Stop(Seconds(100.0));

    Ipv4GlobalRoutingHelper::PopulateRoutingTables();
    
    // Enable pcap tracing
    // Network performance calculation using Wireshark
    // Promiscuous – If true capture all possible packets available at the device.
    bool prom = true; 
    //csma.EnablePcapAll("myFirstAssignment", prom);
    csma.EnablePcap("myFirstAssignment", csmaDevices.Get(0), prom);

    // Network performance calculation using flow monitor
    //Install FlowMonitor on all nodes
    FlowMonitorHelper flowmon;
    Ptr<FlowMonitor> monitor = flowmon.InstallAll();
    Simulator::Stop(Seconds(100.0));


    // Now, do the actual simulation.
    NS_LOG_INFO("Run Simulation.");
    Simulator::Run();


    // Network Perfomance Calculation using Flow Monitor
    int j=0;
    float AvgThroughput = 0;
    Time Jitter;
    Time Delay;

    Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
    std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();

    for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator iter = stats.begin (); iter != stats.end (); ++iter)
        {
        Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (iter->first);

    NS_LOG_UNCOND("----Flow ID:" <<iter->first);
    NS_LOG_UNCOND("Src Addr" <<t.sourceAddress << "Dst Addr "<< t.destinationAddress);
    NS_LOG_UNCOND("Sent Packets=" <<iter->second.txPackets);
    NS_LOG_UNCOND("Received Packets =" <<iter->second.rxPackets);
    NS_LOG_UNCOND("Lost Packets =" <<iter->second.txPackets-iter->second.rxPackets);
    NS_LOG_UNCOND("Packet delivery ratio =" <<iter->second.rxPackets*100/iter->second.txPackets << "%");
    NS_LOG_UNCOND("Packet loss ratio =" << (iter->second.txPackets-iter->second.rxPackets)*100/iter->second.txPackets << "%");
    NS_LOG_UNCOND("Delay =" <<iter->second.delaySum);
    NS_LOG_UNCOND("Jitter =" <<iter->second.jitterSum);
    NS_LOG_UNCOND("Throughput =" <<iter->second.rxBytes * 8.0/(iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds())/1024<<"Kbps");

    SentPackets = SentPackets +(iter->second.txPackets);
    ReceivedPackets = ReceivedPackets + (iter->second.rxPackets);
    LostPackets = LostPackets + (iter->second.txPackets-iter->second.rxPackets);
    AvgThroughput = AvgThroughput + (iter->second.rxBytes * 8.0/(iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds())/1024);
    Delay = Delay + (iter->second.delaySum);
    Jitter = Jitter + (iter->second.jitterSum);

    j = j + 1;

    }

    AvgThroughput = AvgThroughput/j;
    NS_LOG_UNCOND("--------Total Results of the simulation----------"<<std::endl);
    NS_LOG_UNCOND("Total sent packets  =" << SentPackets);
    NS_LOG_UNCOND("Total Received Packets =" << ReceivedPackets);
    NS_LOG_UNCOND("Total Lost Packets =" << LostPackets);
    NS_LOG_UNCOND("Packet Loss ratio =" << ((LostPackets*100)/SentPackets)<< "%");
    NS_LOG_UNCOND("Packet delivery ratio =" << ((ReceivedPackets*100)/SentPackets)<< "%");
    NS_LOG_UNCOND("Average Throughput =" << AvgThroughput<< "Kbps");
    NS_LOG_UNCOND("End to End Delay =" << Delay);
    NS_LOG_UNCOND("End to End Jitter delay =" << Jitter);
    monitor->SerializeToXmlFile("myFirstAssignment_flowMonitor.xml", true, true);

    Simulator::Destroy();
    NS_LOG_INFO("Done.");
    return 0;

}